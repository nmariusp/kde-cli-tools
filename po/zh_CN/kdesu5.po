msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-30 00:47+0000\n"
"PO-Revision-Date: 2022-12-13 12:21\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kde-cli-tools/kdesu5.pot\n"
"X-Crowdin-File-ID: 4331\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Funda Wang"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "fundawang@gmail.com"

#: kdesu.cpp:105
#, kde-format
msgid "KDE su"
msgstr "KDE su"

#: kdesu.cpp:107
#, kde-format
msgid "Runs a program with elevated privileges."
msgstr "用提升的权限来运行程序。"

#: kdesu.cpp:109
#, kde-format
msgid "Copyright (c) 1998-2000 Geert Jansen, Pietro Iglio"
msgstr "Copyright (c) 1998-2000 Geert Jansen, Pietro Iglio"

#: kdesu.cpp:110
#, kde-format
msgid "Geert Jansen"
msgstr "Geert Jansen"

#: kdesu.cpp:110
#, kde-format
msgid "Maintainer"
msgstr "维护者"

#: kdesu.cpp:111
#, kde-format
msgid "Pietro Iglio"
msgstr "Pietro Iglio"

#: kdesu.cpp:111
#, kde-format
msgid "Original author"
msgstr "原作者"

#: kdesu.cpp:120
#, kde-format
msgid "Specifies the command to run as separate arguments"
msgstr "指定要作为独立参数运行的命令"

#: kdesu.cpp:121
#, kde-format
msgid "Specifies the command to run as one string"
msgstr "指定要作为字符串运行的命令"

#: kdesu.cpp:122
#, kde-format
msgid "Run command under target uid if <file> is not writable"
msgstr "如果 <file> 不可写，以目标 uid 运行命令"

#: kdesu.cpp:123
#, kde-format
msgid "Specifies the target uid"
msgstr "指定目标 uid"

#: kdesu.cpp:124
#, kde-format
msgid "Do not keep password"
msgstr "不保存密码"

#: kdesu.cpp:125
#, kde-format
msgid "Stop the daemon (forgets all passwords)"
msgstr "停止守护程序(忘记所有密码)"

#: kdesu.cpp:126
#, kde-format
msgid "Enable terminal output (no password keeping)"
msgstr "启用终端输出(不保存密码)"

#: kdesu.cpp:128
#, kde-format
msgid "Set priority value: 0 <= prio <= 100, 0 is lowest"
msgstr "设置优先值：0 <= prio <=100，0 是最低的"

#: kdesu.cpp:129
#, kde-format
msgid "Use realtime scheduling"
msgstr "使用实时调度"

#: kdesu.cpp:130
#, kde-format
msgid "Do not display ignore button"
msgstr "不显示忽略按钮"

#: kdesu.cpp:131
#, kde-format
msgid "Specify icon to use in the password dialog"
msgstr "指定要在密码对话框中使用的图标"

#: kdesu.cpp:132
#, kde-format
msgid "Do not show the command to be run in the dialog"
msgstr "不在对话框中显示要运行的命令"

#: kdesu.cpp:139
#, kde-format
msgctxt ""
"Transient means that the kdesu app will be attached to the app specified by "
"the winid so that it is like a dialog box rather than some separate program"
msgid "Makes the dialog transient for an X app specified by winid"
msgstr "使 X 应用程序对话框的瞬变值(Transient)由其窗口编号指定"

#: kdesu.cpp:141
#, kde-format
msgid "Embed into a window"
msgstr "嵌入到一个窗口"

#: kdesu.cpp:168
#, kde-format
msgid "Cannot execute command '%1'."
msgstr "无法执行命令“%1”。"

#: kdesu.cpp:171
#, kde-format
msgid "Cannot execute command '%1'. It contains invalid characters."
msgstr "无法执行命令“%1”。它包含了非法字符。"

#: kdesu.cpp:245
#, kde-format
msgid "Illegal priority: %1"
msgstr "无效的优先级：%1"

#: kdesu.cpp:266
#, kde-format
msgid "No command specified."
msgstr "没有指定命令。"

#: kdesu.cpp:369
#, kde-format
msgid "Su returned with an error.\n"
msgstr "Su 返回错误。\n"

#: kdesu.cpp:397
#, kde-format
msgid "Command:"
msgstr "命令："

#: kdesu.cpp:406
#, kde-format
msgid "realtime: "
msgstr "实时："

#: kdesu.cpp:410
#, kde-format
msgid "Priority:"
msgstr "优先级："

#: sudlg.cpp:27
#, kde-format
msgid "Run as %1"
msgstr "以 %1 运行"

#: sudlg.cpp:31
#, kde-format
msgid "Please enter your password below."
msgstr "请输入您的密码。"

#: sudlg.cpp:36
#, kde-format
msgid ""
"The action you requested needs <b>root privileges</b>. Please enter "
"<b>root's</b> password below or click Ignore to continue with your current "
"privileges."
msgstr ""
"您请求的操作需要 <b>root 权限</b>。请在下面输入 <b>root</b> 的密码或按忽略按"
"钮来使用当前的权限来继续。"

#: sudlg.cpp:42
#, kde-format
msgid ""
"The action you requested needs <b>root privileges</b>. Please enter "
"<b>root's</b> password below."
msgstr "您请求的操作需要 <b>root 权限</b>。请在下面输入 <b>root</b> 的密码。"

#: sudlg.cpp:49
#, kde-format
msgid ""
"The action you requested needs additional privileges. Please enter the "
"password for <b>%1</b> below or click Ignore to continue with your current "
"privileges."
msgstr ""
"您请求的操作需要额外的权限。请在下面输入 <b>%1</b> 的密码或忽略按钮来使用当前"
"的权限来继续。"

#: sudlg.cpp:56
#, kde-format
msgid ""
"The action you requested needs additional privileges. Please enter the "
"password for <b>%1</b> below."
msgstr "您请求的操作需要额外的权限。请在下面输入 <b>%1</b> 的密码。"

#: sudlg.cpp:79
#, kde-format
msgid "Conversation with su failed."
msgstr "和 su 的通信失败。"

#: sudlg.cpp:86
#, kde-format
msgid ""
"The program 'su' could not be found.<br />Ensure your PATH is set correctly."
msgstr "没有找到程序“su”。<br />请确定您的 PATH 设置正确。"

#: sudlg.cpp:97
#, kde-format
msgid ""
"Permission denied.<br />Possibly incorrect password, please try again.<br /"
">On some systems, you need to be in a special group (often: wheel) to use "
"this program."
msgstr ""
"权限被拒绝。<br />可能是密码错误，请重试。<br />在某些系统上，您需要加入一个"
"特殊的用户组(通常是：wheel)来使用这个程序。"

#: sudlg.cpp:105
#, kde-format
msgid "Internal error: illegal return from SuProcess::checkInstall()"
msgstr "内部错误：SuProcess::checkInstall() 返回了无效数据"
